import 'bootstrap';
import '../styles/main.scss';
import html from '../index.html';
import './babel.js';

// import AuthorizeModal from "./modal/authorize-modal";
import Modal from './modal/modal';

const axios = require('axios').default;

// import Img from './assets/img.jpg';

// const authorizeModal = new AuthorizeModal({
//     enterButtonSelector: '.main-page-sign-in',
//     closeModalButtonSelector: '.modal-sign-in-close',
//     modalFormSelector: '.sign-in-form',
//     modalFormContainerSelector: '.sign-in-modal',
// });

const modal = new Modal( {
    openModalButtonSelector: 'modal-open-btn',
    modalSelector: 'modal',
    closeModalButtonSelector: 'modal-close-btn',
})

console.log(modal)