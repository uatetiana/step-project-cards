export default class Modal {
    defaultConfig = {
        openModalButtonSelector: '.modal-open-btn',
        modalSelector: '.modal',
        closeModalButtonSelector: '.modal-close-btn',
    }
    config;
    openModalBtn;
    closeModalBtn;
    modalContainer;

    constructor(config) {
        this.config = Object.assign({}, this.defaultConfig, this.config);
        this.initialize();
    }

    initialize() {
        this.openModalBtn = document.querySelector(this.config.openModalButtonSelector);
        this.closeModalBtn = document.querySelector(this.config.closeModalButtonSelector);
        this.modalContainer = document.querySelector(this.config.modalSelector);

        this.openModalBtn.addEventListener('click', this.openModal.bind(this));
        this.closeModalBtn.addEventListener('click', this.closeModal.bind(this));
    }

    openModal() {
        this.modalContainer.classList.add('is-shown');
    }

    closeModal() {
        this.modalContainer.classList.remove('is-shown');
    }
}