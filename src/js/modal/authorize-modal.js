export default class AuthorizeModal {
    defaultConfig = {
        enterButtonSelector: '.main-page-sign-in',
        closeModalButtonSelector: '.modal-sign-in-close',
        modalFormSelector: '.sign-in-form',
        modalFormContainerSelector: '.sign-in-modal',
        // signInButtonSelector: 'sign-in-btn',
    };
    config;
    enterBtn;
    closeBtn;
    modalForm;
    modalFormContainer;
    signInBtn;

    constructor(config) {
        this.config = Object.assign({}, this.defaultConfig, this.config);
        this.initialize();
    }

    initialize() {
        this.closeBtn = document.querySelector(this.config.closeModalButtonSelector);
        this.modalFormContainer = document.querySelector(this.config.modalFormContainerSelector);
        console.log(this.modalFormContainer)
        this.enterBtn = document.querySelector(this.config.enterButtonSelector);
        this.modalForm = document.querySelector(this.config.modalFormSelector);
        // this.signInBtn = document.querySelector(this.config.signInButtonSelector);
        //const enterBtn = document.querySelector(this.config.enterButtonSelector);
        this.enterBtn.addEventListener('click', this.optionHandler.bind(this));

        this.modalForm.addEventListener('submit', this.submitHandler.bind(this));
        this.closeBtn.addEventListener('click', this.closeModalHandler.bind(this));

    }

    optionHandler() {
        if (this.enterBtn.innerText === 'Enter') {
            console.log(this.enterBtn.innerText)
            this.openModalHandler();
        } else if (this.enterBtn.innerText === 'Create Visit') {
            this.createVisit(); // создание карточки в модальном окне
        }
    }

    openModalHandler() {
        this.modalFormContainer.classList.add('is-shown');
    }

    submitHandler(event) {
        event.preventDefault();
        const formData = new FormData(this.modalForm);
        this.getLogin(formData);
        console.log(Object.fromEntries(formData))
        this.closeModalHandler();
    }

    // addPostHandler() {
    //     const modalName = this.authorizeBtn.getAttribute('data-modal');
    //     const modal = document.querySelector('.js-modal[data-modal="' + modalName + '"]');
    //     modal.classList.add('is-shown');
    //
    // }

    // formFocusOutHandler() {
    //     const formData = new FormData(this.modalForm);
    //     this.validateTitle(formData);
    // }


    closeModalHandler() {
        this.modalFormContainer.classList.remove('is-shown');
    }

    getLogin(formData) {
        const API = 'http://ajax.test-danit.com/api';

        fetch(`${API}/login`,
            {
                method: "POST",
                body: JSON.stringify(Object.fromEntries(formData)),
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(r => r.json())
            .then(data => {
                console.log(data);

            })
            .catch(error => {
                this.changeLogInButtonToCreate();
                console.log(`Error: ${error}`);
            })
    }
    changeLogInButtonToCreate() {
        this.enterBtn.innerText = 'Create Visit';


    }


    createCard(card) {

    sessionStorage.setItem('token', '3da75931-6082-43b0-b697-468f6477ec22');
    let token = sessionStorage.getItem('token');
        const API = 'http://ajax.test-danit.com/api';

        return fetch( `${API}/cards`,{

            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(card)
        }).then(r => r.json())
            .then(data => {
                console.log(data);

            })
            .catch(error => {
                this.changeLogInButtonToCreate();
                console.log(`Error: ${error}`);
            })
    }

}